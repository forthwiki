
REQUIRE XSLT ~ac/lib/lin/xml/xslt.f
REQUIRE TYPE>STR ~ygrek/lib/typestr.f
REQUIRE tag ~ygrek/lib/xmltag.f
REQUIRE re_split ~ygrek/lib/re/ext.f
REQUIRE FINE-HEAD ~pinka/samples/2005/lib/split-white.f
REQUIRE SPLIT ~pinka/samples/2005/lib/split.f
REQUIRE AsQWord ~pinka/spf/quoted-word.f
REQUIRE /STRING lib/include/string.f

REQUIRE EMPTY? utils.f
REQUIRE parse-wiki-my parse.f

MODULE: convert

MODULE: detail
\ XSLTm leaks
: XSLTm ( `xml `xslfile -- s ) ['] XSLTm CATCH IF 2DROP 2DROP "" ELSE >STR THEN ;

EXPORT

: xml2wiki ( a u -- s ) S" xml2wiki.xsl" XSLTm ;
: xml2html ( a u -- s ) S" xml2html.xsl" XSLTm ;

DEFINITIONS


EXPORT

: wiki2xml my::['] (wiki2xml) TYPE>STR ;

;MODULE

;MODULE

