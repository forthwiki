REQUIRE { lib/ext/locals.f
REQUIRE CEQUAL ~pinka/spf/string-equal.f

: EMPTY? ( a u -- ) NIP 0= ;

: STARTS-WITH? { a1 u1 a2 u2 -- ? }
  u1 u2 < IF FALSE EXIT THEN
  a1 u2 a2 u2 CEQUAL ;
