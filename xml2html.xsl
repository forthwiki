<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output encoding="utf-8" method="html"/>
<!--doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/-->

<!-- main -->
<xsl:template match="article">
  <div class="article">
    <xsl:apply-templates/>
  </div>
</xsl:template>

<xsl:template match="para">
  <p class="para">
    <xsl:apply-templates/>
  </p>
</xsl:template>

<xsl:template match="heading">
  <h1><xsl:value-of select="."/></h1>
</xsl:template>

<xsl:template match="code">
  <pre><code><xsl:value-of select="."/></code></pre>
</xsl:template>

<xsl:template match="bold">
  <span class="bold"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="mono">
  <span class="mono"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="list">
  <ul>
  <xsl:for-each select="listitem">
    <li><xsl:apply-templates/></li>
  </xsl:for-each>
  </ul>
</xsl:template>

<xsl:template match="page">
  <a href="?page_name={@name}">
    <xsl:value-of select="."/>
  </a>
</xsl:template>

<xsl:template match="link">
  <a href="{@href}"><xsl:value-of select="."/></a>
</xsl:template>

</xsl:stylesheet>
