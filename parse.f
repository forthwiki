REQUIRE AsQWord ~pinka/spf/quoted-word.f
REQUIRE tag ~ygrek/lib/xmltag.f
REQUIRE list-all ~ygrek/lib/list/all.f
REQUIRE re_split ~ygrek/lib/re/ext.f
REQUIRE lexer lexer.f

{{ list
nil VALUE stk
: push ( x -- ) stk cons TO stk ;
: check ( -- ) stk empty? ABORT" list empty" ;
: pop ( -- ) check stk DUP cdr TO stk FREE-NODE ;
: stk? ( -- ? ) stk empty? NOT ;
: top ( -- x ) check stk car ;
: has ( x -- ? ) stk LAMBDA{ OVER = } find NIP NIP ;
}}

0 CONSTANT NONE
1 CONSTANT BOLD 
2 CONSTANT MONO
3 CONSTANT PARA
4 CONSTANT LIST

: open { x } 
  x BOLD = IF ." <bold>" EXIT THEN 
  x MONO = IF ." <mono>" EXIT THEN  
  x PARA = IF ." <para>" EXIT THEN
  x LIST = IF ." <list>" EXIT THEN ;
: close { x } 
  x BOLD = IF ." </bold>" EXIT THEN 
  x MONO = IF ." </mono>" EXIT THEN  
  x PARA = IF ." </para>" EXIT THEN
  x LIST = IF ." </list>" EXIT THEN ;

: close-until { limit } BEGIN stk? WHILE top limit <> WHILE top close pop REPEAT THEN ;
: close-with { limit } limit close-until limit close stk? IF pop THEN ;
: close-all NONE close-until ;

: mark { x } 
  stk? NOT IF x open x push EXIT THEN 
  top x = IF x close pop EXIT THEN
  x open x push ;

: re? ( a u re -- n ) re_match? DROP \1 NIP ;

: link_re RE" (\[([^]|]+)(\|([^]]*))?.).*" ;
: autolink_re RE" (((http://|www\.)(\w+(-\w+)*\.)+\w+)(/\S*)*).*" ;

\ if `str is empty leave `alt
\ else leave `str
: EMPTY?THEN ( `str `alt -- `s ) 2>R 2DUP EMPTY? IF 2DROP 2R> ELSE 2R> 2DROP THEN ;
: TRIM FINE-HEAD FINE-TAIL ;

ALSO XMLSAFE

: xml-link-page ( `text `name -- ) TRIM %[ `name $$ ]% `page atag TYPE ;
: xml-link-href ( `text `href -- ) TRIM %[ `href $$ ]% `link atag TYPE ;

MODULE: my

: lex
  | `** match? => 2DROP BOLD mark ;;
  | `|| match? => 2DROP MONO mark ;;
  | link_re re? => 2DROP close-all \4 TRIM \2 TRIM EMPTY?THEN \2 xml-link-page ;;
  | autolink_re re? => 2DROP \1 2DUP xml-link-href ;;
  | _ => TYPE ;;
  ;

: parse-wiki ( a u -- ) plaintags ['] lex lexer close-all ;

: line2xml ( a u -- )
  FINE-HEAD FINE-TAIL
  2DUP EMPTY? IF 2DROP EXIT THEN
  RE" ## (.*)" re_match? IF `heading tag \1 TYPE EXIT THEN
  `para tag \0 parse-wiki
  ;

: (wiki2xml) ( a u -- ) 
   plaintags
   `article tag
  RE" \n" re_split DUP LAMBDA{ STR@ line2xml } list::iter
  ['] STRFREE list::free-with ;

;MODULE

: start-para PARA mark ;

MODULE: phpwiki

: lex ( a u -- )
  | `__ match? => 2DROP BOLD mark ;;
  | link_re re? => 2DROP \4 EMPTY? IF \2 2DUP xml-link-page ELSE \2 \4 xml-link-href THEN ;;
  | autolink_re re? => 2DROP \1 2DUP xml-link-href ;;
  | `%%% match? => 2DROP close-all PARA mark ;;
  | _ => XMLSAFE::TYPE ;;
  ;

: parse ['] lex lexer ;
: parse-line
  RE" !(.*)" re_match? IF close-all START{ `heading tag \1 parse close-all }EMERGE start-para EXIT THEN
  \0 RE" (\*|#)(.*)" re_match? IF LIST has NOT IF LIST mark THEN `listitem tag \2 parse EXIT THEN
  LIST has IF LIST close-with THEN
  \0 parse ;

;MODULE

PREVIOUS \ XMLSAFE

/TEST

REQUIRE TESTCASES ~ygrek/lib/testcase.f

ALSO my

S" **qq**ds<'>dq||www***eee||rrr[artic**>le|yep]d||sds" my::parse-wiki

TESTCASES link_re
(( S" [dsds]abc" link_re re? -> 6 ))
(( S" [link|text]qqq" link_re re? \4 `text CEQUAL -> 11 TRUE ))
(( S" []" link_re re? -> 0 ))
(( S" [a]" link_re re? \4 EMPTY? -> 3 TRUE ))
(( S" [|]" link_re re? -> 0 ))
(( `[a|] link_re re? \4 EMPTY? -> 4 TRUE ))
(( `[a||] link_re re? \2 `a CEQUAL \4 `| CEQUAL -> 5 TRUE TRUE ))
END-TESTCASES 

TESTCASES autolink_re
(( S" http://forth.org.ru/ page" autolink_re re? -> 20 ))
END-TESTCASES
