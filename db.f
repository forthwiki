: sqlite3_status 2DROP 2DROP -1023 ;
REQUIRE db3_open ~ygrek/lib/db/sqlite3.f
REQUIRE LAMBDA{ ~pinka/lib/lambda.f
REQUIRE NUMBER ~ygrek/lib/parse.f
REQUIRE xml phpwiki.f
REQUIRE storage storage.f
REQUIRE ICONV ~ac/lib/lin/iconv/iconv.f

0 VALUE db 
0 VALUE id
0 VALUE name

: each ( x y pp -- ? )
   NIP NIP { pp | ver }
   0 pp db3_coli -> ver
   " select content from version where id={id} and version={#ver}" STR@ db db3_gets
   xml STR@ 
   name STR@ >UTF8
   ver .
   2DUP TYPE CR
   storage::store 
   TRUE ;

: each ( x y pp -- ? )
  NIP NIP { pp }
    1 pp db3_colu >STR TO name
    0 pp db3_coli TO id
    " select version from version where id={id} order by version asc" STR@ ['] each db sql.enum
  TRUE ;

: main ( a u -- )
  db3_open TO db
  db 0= ABORT" no db"
  S" select id,name from page" ['] each db sql.enum
  db db3_close
  0 TO db
  ;
  
`data.db main BYE
