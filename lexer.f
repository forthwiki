REQUIRE STARTS-WITH? utils.f
REQUIRE { lib/ext/locals.f
REQUIRE /TEST ~profit/lib/testing.f
REQUIRE LAMBDA{ ~pinka/lib/lambda.f
REQUIRE /GIVE ~ygrek/lib/parse.f
REQUIRE PRO ~profit/lib/bac4th.f
REQUIRE re_match? ~ygrek/lib/re/re.f
REQUIRE FINE-HEAD ~pinka/samples/2005/lib/split-white.f

: | 2DUP ;
: => S" ?DUP IF /GIVE" EVALUATE ; IMMEDIATE
: ;; S" EXIT THEN" EVALUATE ; IMMEDIATE

: match? ( a u `str -- n ) DUP { n } STARTS-WITH? IF n ELSE 0 THEN ;
: _ 2DROP 1 ;

: lexer ( `str xt -- ) { xt } BEGIN xt EXECUTE DUP 0= UNTIL 2DROP ;
: }LEXER S" } lexer" EVALUATE ; IMMEDIATE

\ : lexer=> ( `str xt --> i*x \ <-- ) PRO { xt } BEGIN xt EXECUTE CONT DUP 0= UNTIL 2DROP ;

/TEST

REQUIRE AsQWord ~pinka/spf/quoted-word.f
REQUIRE TESTCASES ~ygrek/lib/testcase.f

: test
  LAMBDA{
  | `+ match? => 2DROP 1 % ;;
  | `- match? => 2DROP -1 % ;;
  | _ => 2DROP 0 % ;; 
  } %[ lexer ]% 0 SWAP ['] + list::iter ;

TESTCASES lexer
(( S" ++--ewew++" test -> 2 ))
(( S" **dsds**er**dqew" test -> 0 ))
END-TESTCASES

