REQUIRE STR@ ~ac/lib/str5.f
REQUIRE re_split ~ygrek/lib/re/ext.f
REQUIRE OCCUPY ~pinka/samples/2005/lib/append-file.f
REQUIRE TYPE>STR ~ygrek/lib/typestr.f

S" parse.f" INCLUDED

: xml-declaration " <?xml version={''}1.0{''} encoding={''}windows-1251{''}?>" STYPE CR ;

ALSO XMLSAFE

: BACKSTRLISTFREE ( sl --> sl \ <-- ) PRO BACK ['] STRFREE list::free-with TRACKING RESTB CONT ;

: para2xml ( a u -- )
  TRIM
  2DUP EMPTY? IF 2DROP EXIT THEN
  start-para
  RE" \n" re_split BACKSTRLISTFREE LAMBDA{ STR@ phpwiki::parse-line } list::iter
  close-all
  ;

: (wiki2xml) ( a u -- )
   xml-declaration
   plaintags
   `article tag
   RE" \r?\n\r?\n" re_split BACKSTRLISTFREE LAMBDA{ STR@ para2xml } list::iter ;

PREVIOUS \ XMLSAFE

: wiki2xml ['] (wiki2xml) TYPE>STR ;

: xml ( `filename -- s ) >STR DUP " \n" " {EOLN}" replace-str- STR@ wiki2xml ;

\ `lisp.wiki FILE xml STR@ `Lisp.xml OCCUPY
\ `haskell.wiki FILE xml STR@ `Haskell.xml OCCUPY

\ `phpwiki.test FILE xml STYPE

\ BYE
