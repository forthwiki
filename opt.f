
lib/ext/spf-asm.f
lib/ext/disasm.f

MODULE: ASSEMBLER
MODULE: ASM-HIDDEN
EXPORT
: FIN-CODE END-ASM ?FINISHED ?UNRES ?CSP RESTORE-CURRENT CODE-ALIGN EXIT-ASSEMBLER ;
;MODULE
;MODULE

: test ( a u -- ) 
  2>R
  ALSO ASSEMBLER 
  !CSP
  ASSEMBLER::INIT-ASM 
  2R> EVALUATE 
  ASSEMBLER::FIN-CODE 
  PREVIOUS ;

: q S" MOV ECX, # 8 CDQ IDIV ECX " ;

\ : q 8 / ;
