REQUIRE STR@ ~ac/lib/str5.f
REQUIRE new-hash ~pinka/lib/hash-table2.f
REQUIRE cat ~ygrek/lib/cat.f
REQUIRE AsQWord ~pinka/spf/quoted-word.f
REQUIRE OCCUPY ~pinka/samples/2005/lib/append-file.f
REQUIRE MD5 ~clf/md5-ts.f
REQUIRE LAY-PATH ~pinka/samples/2005/lib/lay-path.f \ creation of path directories
REQUIRE NUMBER ~ygrek/lib/parse.f
REQUIRE list-make ~ygrek/lib/list/make.f
REQUIRE list-ext ~ygrek/lib/list/ext.f
REQUIRE FileLines=> ~ygrek/lib/filelines.f

MODULE: storage
: datadir S" data" ;
;MODULE

[UNDEFINED] WINAPI: [IF]
REQUIRE READDIR ~ygrek/lib/linux/findfile.f
WARNING @ WARNING 0!
: FIND-FILES-R FIND-FILES ;
WARNING !
USER FIND-FILES-DEPTH
[THEN]
REQUIRE ITERATE-FILES ~profit/lib/iterate-files.f

\ TODO locking

: /PAD 1024 1- ;
: STR>PAD ( s -- a u )
  DUP STR@ /PAD MIN >R PAD R@ CMOVE
  STRFREE
  PAD R> 2DUP + 0 SWAP C! ;

: FORCE-PATH-CATCH 2DUP LAY-PATH-CATCH ;

MODULE: storage
MODULE: detail

: fullpath ( `path -- `path' ) datadir " {s}/{s}" STR>PAD ;
: dirname ( `name -- `dir ) MD5 fullpath ;
: name-version-file ( `name version -- `file ) -ROT dirname " {s}/{n}" STR>PAD ;
: update-catalog ( `name n -- `name n )
  DUP 0 <> IF EXIT THEN \ no need to update - it is already there
  >R
  2DUP `catalog fullpath FORCE-PATH-CATCH DROP ATTACH-LINE-CATCH DROP \ FIXME logging here
  R> ;

EXPORT

\ NB instead of CUT-FILENAME and FIND-FILES better use simply READDIR but it is not implemented on Win32
: get-versions ( `name -- nl ) dirname LAMBDA{ NIP IF 2DROP EXIT THEN CUT-FILENAME NUMBER IF % THEN } %[ FIND-FILES ]% ;
: latest-version ( `name - n ) get-versions 0 OVER ['] MAX list::iter SWAP list::free ;
: store ( `content `name -- ) 2DUP latest-version update-catalog 1+ name-version-file
  FORCE-PATH-CATCH DROP OCCUPY-CATCH DROP ; \ FIXME logging here

: exists ( `name -- ? ) dirname FILE-EXIST ;
: get-xml-version ( `name version -- h TRUE | FALSE )
   name-version-file __cat IF STRFREE FALSE EXIT THEN
   ( s ) small-hash { s h }
   s STR@ `content h HASH!
   s STRFREE
   h TRUE ;

: get-xml-latest ( `name -- h TRUE | FALSE )
   2DUP latest-version ?DUP 0 = IF 2DROP FALSE EXIT THEN
   get-xml-version ;

: all=> ( --> a u \ <-- ) PRO `catalog fullpath FileLines=> DUP STR@ CONT ;

;MODULE
;MODULE

/TEST

ALSO storage
\ `page detail::dirname :NONAME . . TYPE CR ; READDIR
\ `page get-versions

\ `test1 `page store
\ `test2 `page store
\ `page exists .
\ `page latest-version .

`MainPage.xml FILE `MainPage store
`article.xml FILE `article store
`Rules.xml FILE `Rules store
