<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output encoding="utf-8" method="text"/>

<!-- main -->
<xsl:template match="article">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="para">
  <xsl:text>&#x0A;</xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="heading">
  <xsl:text>&#x0A;## </xsl:text>
  <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="code">
  <pre><code><xsl:value-of select="."/></code></pre>
</xsl:template>

<xsl:template match="bold">
  <xsl:text>**</xsl:text><xsl:value-of select="."/><xsl:text>**</xsl:text>
</xsl:template>

<xsl:template match="mono">
  <xsl:text>||</xsl:text><xsl:value-of select="."/><xsl:text>||</xsl:text>
</xsl:template>

<xsl:template match="list">
  <xsl:for-each select="listitem">
    <xsl:text>&#x0A;* </xsl:text><xsl:apply-templates/>
  </xsl:for-each>
</xsl:template>

<xsl:template match="page">
  <xsl:choose>
  <xsl:when test="@name=.">
    <xsl:text>[</xsl:text><xsl:value-of select="@name"/><xsl:text>]</xsl:text>
  </xsl:when>
  <xsl:otherwise>
    <xsl:text>[</xsl:text><xsl:value-of select="@name"/><xsl:text>|</xsl:text><xsl:value-of select="."/><xsl:text>]</xsl:text>
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
