\ forthwiki

REQUIRE ATTACH ~pinka/samples/2005/lib/append-file.f
REQUIRE USER-TYPE ~ygrek/lib/typestr.f
\ :NONAME S" wiki.err" ATTACH ; TO USER-TYPE \ no stdout

REQUIRE XSLTmm ~ac/lib/lin/xml/xslt.f
REQUIRE XHTML ~ygrek/lib/xhtml/core.f
REQUIRE DumpParams ~ac/lib/string/get_params.f
REQUIRE EQUAL ~pinka/spf/string-equal.f
REQUIRE NOT ~profit/lib/logic.f
REQUIRE cat ~ygrek/lib/cat.f
REQUIRE ALLOCATED ~pinka/lib/ext/basics.f
REQUIRE NUMBER ~ygrek/lib/parse.f
REQUIRE [env] ~ygrek/lib/env.f
REQUIRE DateTime>PAD ~ygrek/lib/spec/unixdate.f
REQUIRE FileLines=> ~ygrek/lib/filelines.f
REQUIRE READ-FILE-EXACT ~pinka/lib/files-ext.f
REQUIRE STRAPPEND ~ygrek/lib/str.f
REQUIRE BACKSTR@ ~ygrek/lib/backstr.f
REQUIRE CGI ~ygrek/lib/net/cgi.f
REQUIRE XHTML-EXTRA ~ygrek/lib/xhtml/extra.f

S" storage.f" INCLUDED
S" convert.f" INCLUDED

2048 VALUE next-exn-id
: exception ( `text -- id ) 2DROP next-exn-id DUP 1+ TO next-exn-id ;
: exception: ( "name" -- ) PICK-NAME exception CONSTANT ;

exception: #forbidden

: ENVIR ENVIRONMENT? NOT IF S" " THEN ;

: EMIT-URLENCODED ( c -- ) '%' EMIT BASE @ >R HEX S>D <# # # #> TYPE R> BASE ! ;

\ as per RFC 1738
: URLENCODE-CHAR ( c -- )
  SP@ 1 RE" [a-zA-Z0-9$_.+!*'(),-]" re_match? IF EMIT ELSE EMIT-URLENCODED THEN ;

: urlencode ( a u -- s ) LAMBDA{ BOUNDS DO I C@ URLENCODE-CHAR LOOP } TYPE>STR ;

: param: ( a u "name" -- )
  PARSE-NAME 2OVER 2OVER
  " : #{s} S{''} {s}{''} ;" STREVALUATE
  " : ${s} S{''} {s}{''} GetParam ;" STREVALUATE ;

`action param: action
`page_name param: name
`special param: special

ALSO XMLSAFE
ALSO XHTML

\ Every page
: <page> ( `title -- )
   PRO
   xml-declaration
   doctype-strict
   xhtml
   << `head tag
     << `application/xhtml+xml;charset=utf-8 `content-type http-equiv >>
     << `title tag ( `title ) TYPE S"  :: ForthWiki" TYPE >>
     << `wiki.css link-stylesheet >>
   >>

   `body tag
   CONT ;

\ ugly?
: +urlencoded ( a u s -- s' ) -ROT urlencode STRAPPEND ;
: page-url ( `name -- s ) #name " ?{s}=" +urlencoded ;
: page-action-url ( `action `name -- s ) page-url -ROT #action " &{s}=" +urlencoded STRAPPEND ;
: page-version-url ( n `name -- s ) page-url SWAP `version " &{s}={n}" STRAPPEND ;
: special-url ( `name -- s ) #special " ?{s}=" +urlencoded ;

: link-page ( `name -- ) 2DUP page-url link-s TYPE ;
: link-page-action ( `text `action `name -- ) page-action-url link-s TYPE ;
: link-special ( `name -- ) 2DUP special-url link-s TYPE ;
: link-version-tag ( n `name --> \ <-- ) PRO page-version-url link-s CONT ;

\ wrap wiki page
: <wiki> ( `name -- )
  PRO
  [env] `name env!
  `name env@ <page>
  << `pagename :span `name env@ TYPE >>
  hrule
  << `toolbar :div
    `View `view `name env@ link-page-action
    `Edit `edit `name env@ link-page-action
    `History `history `name env@ link-page-action
  >>
  hrule
  << `warning :div
    S" This wiki is experimental. Expect your changes to be lost some day." TYPE
  >>
  << `quick :div
    ul
    << li `MainPage link-page >>
    << li `Rules link-page >>
    << li `catalog link-special >>
  >>
  << `page-sep-left :div >>
  << `page :div CONT >>
  << `page-sep-bottom :div >>
  << `footer :div S" version 0" TYPE >> ;

: render-not-found ( `name -- )
  [env] `name env!
  `name env@ " No such article : {s}" STYPE CR
  S" Do you wish to " TYPE `create `edit `name env@ page-action-url BACKSTR@ link-text S"  it?" TYPE ;

\ continue only if such page exists
\ render not-found page if no such page
\ : notfound|| ( `name --> `name \ <-- )
\   PRO
\   2DUP storage::exists IF CONT ELSE render-not-found THEN ;

: render-edit ( a u -- )
  \ << `h1 tag S" Nota bene: Editing is disabled ('save' will ignore your changes)" TYPE >>
  $name page-url BACKSTR@ form-post
  `div tag
  `save #action `hidden input
  $name #name `hidden input
  ms@ " {n}" BACKSTR@ `page_time `hidden input

  <<
  %[ `content `name $$ `25 `rows $$ `80 `cols $$ ]% `textarea atag
  ( a u ) TYPE
  >>

  `div tag
  CR
  S$ |S" 3 DUP *" EVALUATE| TYPE S" " `answer `text input
  CR CR
  `SAVE `button `submit input
  `PREVIEW `button `submit input ;

: render-show { a u version | h -- }
  version 0 = IF S" latest" TYPE CR a u storage::get-xml-latest ELSE " version: {#version}" STYPE CR a u version storage::get-xml-version THEN ( h ? )
  IF -> h
   `content h HASH@ convert::xml2html FORTH::STYPE
   h del-hash
  ELSE
   version 0 = IF a u render-not-found ELSE a u 0 RECURSE THEN
  THEN ;

: edit-page-latest ( `name -- )
  storage::get-xml-latest IF
  { h | s }
  `content h HASH@ convert::xml2wiki -> s
  s STR@ render-edit
  s STRFREE
  h del-hash
  ELSE
   S" " render-edit
  THEN ;

: render-history ( -- )
  ul
  $name storage::get-versions ['] < list::sorted
  LAMBDA{ li DUP $name link-version-tag . } list::free-with ;

: render-preview ( a u -- )
  << `h3 tag `Preview TYPE >>
  2DUP convert::wiki2xml BACKSTR@ convert::xml2html FORTH::STYPE
       render-edit ;

: render-tarpit
  `REMOTE_ADDR ENVIR " {s} is a bot" BACKSTR@ `wiki.log ATTACH-LINE-CATCH DROP
  #forbidden THROW
  << `h1 tag S" Please learn Forth language before editing ForthWiki" TYPE >> ;

: wiki-save ( -- )
  `button GetParam `PREVIEW CEQUAL IF `content GetParam render-preview EXIT THEN
  `button GetParam `SAVE CEQUAL IF
  ( `page_time GetParam NUMBER IF
      ms@ - ABS 4000 < IF render-tarpit EXIT THEN)
      `answer GetParam FINE-HEAD FINE-TAIL `9 CEQUAL NOT IF render-tarpit EXIT THEN
      `content GetParam convert::wiki2xml BACKSTR@ $name storage::store
  THEN
  $name 0 render-show ;

\ : wiki-save1 ( -- )
\  `Save <wiki>
\  S" Editing is disabled for unauthorized visitors!" TYPE ;

: GetParamInt ( `str -- n ) GetParam NUMBER NOT IF 0 THEN ;

: wiki-article ( -- )
  $name <wiki>
  $action `edit CEQUAL IF $name edit-page-latest EXIT THEN
  $action `save CEQUAL IF wiki-save EXIT THEN
  $action `history CEQUAL IF render-history EXIT THEN
  $name `version GetParamInt render-show ;

: special-page-catalog
  S" :: Catalog" <page>
  ul
  storage::all=> li link-page ;

: wiki-main ( -- )
  $special `catalog CEQUAL IF special-page-catalog EXIT THEN
  S" :: Default" <page>
  S" Go to " TYPE `MainPage link-page CR
  S" Or browse all articles : " TYPE `catalog link-special CR
(  hrule
  `article 0 render-show
  hrule
  `Rules 0 render-show
  hrule)
\  S" Or browse articles extracted from the wiki.forth.org.ru database :" TYPE CR
\  `data -1 ITERATE-FILES
\    NIP IF 2DROP ELSE
\    RE" (.*)\.xml" re_match? IF \1 link-page CR THEN THEN
;

PREVIOUS
PREVIOUS

: TAB 0x09 EMIT ;

: log_request
  LAMBDA{
  TIME&DATE DateTime>PAD TYPE TAB
  `REMOTE_ADDR ENVIR TYPE TAB
  `REQUEST_METHOD ENVIR TYPE TAB
  `SCRIPT_NAME ENVIR TYPE SPACE
  `QUERY_STRING ENVIR TYPE TAB
  `HTTP_USER_AGENT ENVIR TYPE
  } TYPE>STR BACKSTR@ `wiki.log ATTACH-LINE-CATCH DROP ;

ALSO CGI

: headers
  content:xhtml
  S" Cache-Control: no-cache" TYPE CR ;

: content
  log_request
  get-params
  $name EMPTY? IF wiki-main ELSE wiki-article THEN
  CR ;

: index
  ['] content TYPE>STR-CATCH ( s exn )
  DUP
  IF
    NIP
    content:text
    #forbidden = IF 403 status " Forbidden" ELSE " Internal error" THEN
  ELSE
    DROP
    headers
    DUP STRLEN content-length
  THEN
  CR STYPE
  BYE ;

PREVIOUS

\ : index headers CR content BYE ;

\ : REQUEST_METHOD S" GET" ;
\ : QUERY_STRING S" page_name=MainPage" ;

\ ' TYPE1 TO USER-TYPE

: save ['] index MAINX ! `wiki.cgi SAVE ;

